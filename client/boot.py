# This file is executed on every boot (including wake-boot from deepsleep)
#import esp
#esp.osdebug(None)
import gc
#import webrepl
#webrepl.start()
gc.collect()

# Disable the built-in AP
import network
ap_if = network.WLAN(network.AP_IF)
if ap_if.active():
    ap_if.active(False)

from main import connect_to_wifi, report_error, load_config, get_weather

config = load_config()
ssid = config['wifi']['ssid']
password = config['wifi']['password']

try:
    connect_to_wifi(ssid, password)
except:
    report_error()
