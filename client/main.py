import network
import machine
import time
import ujson
import si7021

DOT_LENGTH = 0.1
DASH_LENGTH = 0.25
CHAR_SPACE = 0.1

led = machine.Pin(2, machine.Pin.OUT)


def load_config():
    fh = open('config.json', 'r')
    return ujson.loads(fh.read())


def connect_to_wifi(ssid, password):
    sta_if = network.WLAN(network.STA_IF)
    sta_if.active(True)
    sta_if.connect(ssid, password)
    on = True
    while not sta_if.isconnected():
        dot()
    led.on()  # turn LED off


def read_weather():
    """
    Reads temperature data via si7021, assuming SCL on D5 (GPIO 14) and SDA on D6 (GPIO 12).
    """
    i2c = machine.I2C(scl=machine.Pin(14), sda=machine.Pin(12))
    s = si7021.SI7021(i2c)
    s.reset()  # maybe unnecessary?
    time.sleep_ms(20)
    return s.temperature(), s.humidity()


def read_weathers(count):
    temps = []
    humids = []
    failures = 0
    while len(temps) < count:
        try:
            temp, humidity = read_weather()
            temps.append(temp)
            humids.append(humidity)
        except:
            failures += 1
            if failures > 10:
                report_error()
                raise Exception('Too many read failures')
        time.sleep(1)
    temps.sort()
    humids.sort()
    return temps, humids


def get_weather(count=8):
    """
    Reads temps/humidity several times, returning the average of middle half of readings,
    discarding outliers.
    """
    temps, humids = read_weathers(count)
    middle_temps = temps[count//4:-count//4]
    middle_humids = humids[count//4:-count//4]

    avg_temp = sum(middle_temps) / len(middle_temps)
    avg_humid = sum(middle_humids) / len(middle_humids)

    return avg_temp, avg_humid


def dot():
    led.off()
    time.sleep(DOT_LENGTH)
    led.on()
    time.sleep(CHAR_SPACE)

def dash():
    led.off()
    time.sleep(DASH_LENGTH)
    led.on()
    time.sleep(CHAR_SPACE)

def blink_sos():
    for i in range(3):
        dot()
    time.sleep(CHAR_SPACE)
    for i in range(3):
        dash()
    time.sleep(CHAR_SPACE)
    for i in range(3):
        dot()
    time.sleep(CHAR_SPACE * 2)

def report_error():
    for i in range(3):
        blink_sos()
