# tinysnakes
MicroPython tutorial materials (to be debuted at Open Source Bridge 2017 in the session "[Capturing Tiny Snakes](http://opensourcebridge.org/sessions/2079)")

* [Lesson 1: Flash and Blink](Lesson1.md)
* [Lesson 2: Wifi](Lesson2.md)
* [Lesson 3: Working with a sensor](Lesson3.md)


Notes:

* Currently uses SI7021 temp/humidity sensor (si7021.py)
* Might be able to read state of battery via voltage divider on pin A0, see http://randomnerdtutorials.com/esp8266-adc-reading-analog-values-with-nodemcu/
* Might be also able to read internal temp of MCU for comparison?





