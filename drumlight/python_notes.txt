Interrupts (analog comparator) in micropython.

https://docs.micropython.org/en/latest/reference/isr_rules.html

- use micropython.schedule to run something after the interrupt, if it has to do more than twiddle already-allocated memory
- reentrancy is vital; set a global flag that an interrupt is being processed and check that flag first thing

see also:
https://randomnerdtutorials.com/micropython-interrupts-esp32-esp8266/

- everything but GPIO16 can be used as interrupt
- Pin.IRQ_FALLING or Pin.IRQ_RISING might work instead of analog comparator?  But might require an amplifier to get it to 3.3v

see also:
https://forum.micropython.org/viewtopic.php?t=3442
- use a schmitt trigger?
- use an op-amp like a TLV2472

https://forum.micropython.org/viewtopic.php?t=8283&p=47206
-- trying to use ADC as interrupts; at this point, this requires custom register hacking or C code
-- additionally, the configuration of that ADC happens at flash time, making this tricky


Conclusions:
Try polling.  It might work.  Test it while system also under wifi load, like a repeating task to hit up a URL.
