Experiments on repique:

- mount it face up on top head (air hole and foam squished against clip)
-- rings for ~20ms with reasonably fast drop, could just delay for 25ms before re-triggering
-- very uncertain readings, though that could be oscilloscope
-- ~.8v peak-to-trough, .4v peaks
- mount it face up on resonant head (air hole and foam squished against clip)
-- less tonal change this way
-- ~.8v peak-to-trough, .4v peaks
--- continues ringing a lot longer, up to 40ms before voltage drops below half

- try drilling a hole in clip to allow sound to escape
-- this seems to increase ringing decay rate

- potentiometer in parallel with piezo reduces seen voltage, maybe smooths slightly?
-- ~4k appears to cut peaks roughly in half, so a 10k audio pot is probably ideal
