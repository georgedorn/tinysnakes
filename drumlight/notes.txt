Useful links:

https://pubweb.eng.utah.edu/~cs5789/handouts/piezo.pdf  see end about how ringing happens.

https://docs.micropython.org/en/latest/esp8266/tutorial/adc.html  how to do ADC in micropython / esp8266

https://randomnerdtutorials.com/esp8266-adc-reading-analog-values-with-nodemcu/  More about voltage dividing for esp8266.
https://www.letscontrolit.com/wiki/index.php/DC_Voltage_divider yet more esp8266 voltage divider


More ideas:

Physical muffling and tuning of piezo sensor - 
try coating a piezo in epoxy or plasti-dip to reduce gain
adjust compression on piezo to pre-load it - e.g. with a load-adjustment knob/bolt

other piezo options - film and cable - maybe try other sizes
maybe don't mount on the head

Discrete component adjustments:
clip signal with a pair of diodes in opposite directions?
try tying input signal to ground, maybe via potentiometer?
home-grown piezo crystals?
maybe shape the sound, treat it like audio? - low pass or high pass or band pass the signal
.022uf caps is common for guitar cutoff, .1uf useful for other audio
msgeq7 - https://www.sparkfun.com/datasheets/Components/General/MSGEQ7.pdf - level analysis in hardware


Software adjustments:
arduino example - smoothing
fft in software to do level analysis
mute piezo for ~5-10 ms after a hit, get the internal vibrations in piezo to stop before that time

See also:
pins pegs pickups (for hardware)
controllerists - group in bay area



Program control:
- Look into streaming ACN for UDP lighting data; this might be overkill, or used just for certain non-sound-reactive effects.  
- https://kno.wled.ge is ESP32 firmware with wifi/streaming built in, might be a good option
- https://kno.wled.ge/interfaces/udp-realtime/
