max_value_seen = 0


def get_piezo():
  """
  Only pin 0 works for ADC, apparently.
  """
  import machine
  pin = machine.ADC(0)
  return pin


def read_forever(pin):
  global max_value_seen
  while True:
    val = get_peak(pin)    

    if val > max_value_seen * 0.5:
      print("Hit!", val)
      eat_ringing(pin, val)


def eat_ringing(pin, val):
  new_val = pin.read()
  while new_val > 10 and new_val > val * 0.5:
    new_val = pin.read()


def get_peak(pin):
  global max_value_seen
  val = pin.read()
  new_val = pin.read()
  while new_val > val:
    val = new_val
    new_val = pin.read()
  max_value_seen = max(val, max_value_seen)
  return val


def go():
  p = get_piezo()
  read_forever(p)
